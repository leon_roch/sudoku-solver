﻿using System;
using NUnit.Framework;
using Sudoku;

namespace Testing
{
    [TestFixture]
    public class Tests
    {
        private readonly int[,] _grid = new int[,]
        {
            {7, 4, 0, 0, 3, 0, 0, 1, 0},
            {0, 1, 9, 0, 6, 8, 5, 0, 2},
            {0, 0, 0, 0, 0, 4, 3, 0, 0,},
            {0, 5, 6, 3, 7, 0, 0, 0, 1},
            {0, 0, 1, 8, 0, 0, 0, 9, 5,},
            {0, 9, 0, 0, 2, 0, 6, 0, 0,},
            {1, 0, 3, 4, 0, 7, 2, 0, 0},
            {5, 0, 0, 2, 0, 0, 0, 0, 8,},
            {0, 8, 0, 0, 0, 1, 4, 7, 0}
        };
        
        
        [Test]
        public void Test1()
        {
            Assert.True(true);
        }
        
        [Test]
        public void ValidSudokuGrid()
        {

            var solver = new Solver(_grid);
            Assert.True(solver.IsValid( 0, 2, 2));
            Assert.False(solver.IsValid( 0, 2, 7));

        }

        [Test]
        public void SudokuFilled()
        {
            var solver = new Solver(_grid);
            solver.Solve( 0, 0);
            foreach (var row in solver.Grid)
            {
                Assert.NotZero(row);
            }
        }
    }
}