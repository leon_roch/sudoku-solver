﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku
{
    public class Solver
    {
        private int[,] _grid;
        private readonly Random _random = new Random();

        public int[,] Grid => _grid;


        public Solver(int[,] grid)
        {
            _grid = grid;
        }

        public bool Solve( int x, int y)
        {
            // check if grid is filled
            if (x == 8 && y == 9) return true;

            // check if collum is done
            if (y >= 9) return Solve( x + 1, 0);
            
            // check if cell is filled
            if(_grid[x, y] != 0) return Solve( x , y+1);
            
            // try 1 - 9 in cell (x,y)
            for (int i = 1; i < 10; i++)
            {
                if (!IsValid( x, y, i)) continue;
                
                // assign 'i' to cell (x,y)
                _grid[x, y] = i;

                // try next cell if all cells are valid last one returns true
                if (Solve( x, y + 1)) return true;
            }
            
            // because code has gotten here there's no valid num so reset cell to '0'
            _grid[x, y] = 0;
            
            return false;
        }

        public bool IsValid( int x, int y, int num)
        {
            // check if col 'y' would be valid with 'num'
            for (int row = 0; row < 9; row++)
            {   
                if (_grid[row, y] == num) return false;
            }
            
            // check if row 'x' would be valid with 'num'
            for (int col = 0; col < 9; col++)
            {
                if (_grid[x, col] == num) return false;
            }
            
            // check if num would be valid in box
            var startRow = x - x % 3;
            var startCol = y - y % 3;
            
            // + 3 because Box size is 3 * 3
            for (int row = startRow; row < startRow + 3; row++)
            {
                for (int col = startCol; col < startCol + 3; col++)
                {
                    if (_grid[row, col] == num) return false;
                }
            }
            
            return true;
        }

        public void Print()
        {
            for (int i = 0; i < 9; i++)
            {
                var str = new StringBuilder();
                
                for (int j = 0; j < 9; j++)
                {
                    str.Append(_grid[i, j] + " ");
                }
                Console.WriteLine(str);
            }
        }
    }
}