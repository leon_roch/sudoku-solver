﻿using System;

namespace Sudoku
{
    internal class Program
    {
        
        static readonly int[,] _grid = new int[,]
        {
            {7, 4, 0, 0, 3, 0, 0, 1, 0},
            {0, 1, 9, 0, 6, 8, 5, 0, 2},
            {0, 0, 0, 0, 0, 4, 3, 0, 0},
            {0, 5, 6, 3, 7, 0, 0, 0, 1},
            {0, 0, 1, 8, 0, 0, 0, 9, 5},
            {0, 9, 0, 0, 2, 0, 6, 0, 0},
            {1, 0, 3, 4, 0, 7, 2, 0, 0},
            {5, 0, 0, 2, 0, 0, 0, 0, 8},
            {0, 8, 0, 0, 0, 1, 4, 7, 0}
        };
        public static void Main(string[] args)
        {
            var solver = new Solver(_grid);
            
            solver.Solve( 0, 0);
            solver.Print();
        }
    }
}